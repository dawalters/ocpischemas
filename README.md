# A collection of XML Schemas for OpenCPI XML Files

This repository contains a collection of opinionated XML Schemas that ensures
the validity of the associated OpenCPI XML files.

## Table of contents

- [Usage](#usage)
    - [`xmllint`](#xmllint)
    - [`lemminx`](#lemminx)
        - [`nvim`](#nvim)
        - [`code` / `vscode`](#code--vscode)

## Usage

### `xmllint`

`xmllint` lints XML files, providing opinionated improvements to
their formatting and structure, as well as validating the correctness of the
syntax.

An example invocation:

```bash
xmllint --schema <path-to-schema> <file>
```

### `lemminx`

`lemminx` provides standard code editor features for XML files in the form of a
language server. It can also check files against a schema.

#### Relevant settings

- `xml.fileAssociations`
    - A list of objects containing the fields:
        - `systemId`: Path to an XML Schema
            - This must not contain any environment variables.
            - Recommendation: use absolute paths.
        - `pattern`: Files to use the schema on
            - [Warning: Globs must use double asterisks](https://github.com/eclipse/lemminx/issues/1643).

#### `nvim`

Install and perform the initial setup of [`nvim-lspconfig`](https://github.com/neovim/nvim-lspconfig).

An example configuration:

```lua
lspconfig.lemminx.setup {
    on_attach = mappings.on_attach,
    capabilities = capabilities,
    settings = {
        xml = {
            fileAssociations = {
                {
                    systemId = vim.env.HOME .. "/.config/opencpi/schemas/componentspec.xsd",
                    pattern = "**-comp.xml",
                },
            },
            server = {
                workDir = vim.env.HOME .. "/.cache/lemminx",
            },
        },
    },
}
```

#### `code` / `vscode`

Install the [Red Hat XML extension](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml).

An example configuration (in the `~/.config/Code\ -\ OSS/User/settings.json`
file):

```json
{
    "xml.fileAssociations": [
        {
            "systemId": "/home/<user>/.config/opencpi/schemas/componentspec.xsd",
            "pattern": "**-comp.xml"
        }
    ]
}
```
